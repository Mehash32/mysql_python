import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot"
)

mycursor = mydb.cursor()

"""
#create a database named "mydatabase":
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot"
)

mycursor = mydb.cursor()

mycursor.execute("CREATE DATABASE mydatabase")
"""
"""
#Try connecting to the database "mydatabase":
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot",
  database="mydatabase"
)
"""
"""
#Create a table named "customers":
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot",
  database="mydatabase"
)

mycursor = mydb.cursor()

mycursor.execute("CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))")
"""
"""
#Create primary key on an existing table:

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot",
  database="mydatabase"
)

mycursor = mydb.cursor()

mycursor.execute("CREATE TABLE customers (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), address VARCHAR(255))")
"""
"""
#Create primary key on an existing table:

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="root",
  database="mydatabase"
)

mycursor = mydb.cursor()

mycursor.execute("ALTER TABLE customers ADD COLUMN id INT AUTO_INCREMENT PRIMARY KEY")
"""
"""
#To fill a table in MySQL, use the "INSERT INTO" statement
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot",
  database="mydatabase"
)

mycursor = mydb.cursor()

sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = ("John", "Highway 21")
mycursor.execute(sql, val)

mydb.commit()

print(mycursor.rowcount, "record inserted.")

#Important!: Notice the statement: mydb.commit(). It is required to make the changes, otherwise no changes are made to the table.
"""
"""
#To insert multiple rows into a table, use the executemany() method.

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot",
  database="mydatabase"
)

mycursor = mydb.cursor()

sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = [
  ('Peter', 'Lowstreet 4'),
  ('Amy', 'Apple st 652'),
  ('Hannah', 'Mountain 21'),
  ('Michael', 'Valley 345'),
  ('Sandy', 'Ocean blvd 2'),
  ('Betty', 'Green Grass 1'),
  ('Richard', 'Sky st 331'),
  ('Susan', 'One way 98'),
  ('Vicky', 'Yellow Garden 2'),
  ('Ben', 'Park Lane 38'),
  ('William', 'Central st 954'),
  ('Chuck', 'Main Road 989'),
  ('Viola', 'Sideway 1633')
]

mycursor.executemany(sql, val)

mydb.commit()

print(mycursor.rowcount, "was inserted.")
"""
"""
#Select all records from the "customers" table, and display the result:
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot",
  database="mydatabase"
)

mycursor = mydb.cursor()

mycursor.execute("SELECT * FROM customers")

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

  """

  """
  #Select only the name and address columns:
  mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot",
  database="mydatabase"
)

mycursor = mydb.cursor()

mycursor.execute("SELECT name, address FROM customers")

myresult = mycursor.fetchall()

for x in myresult:
  print(x)
  """
  """
  #If you are only interested in one row, you can use the fetchone() method.
  mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot",
  database="mydatabase"
)

mycursor = mydb.cursor()

mycursor.execute("SELECT * FROM customers")

myresult = mycursor.fetchone()

print(myresult)
"""
"""
#Select record(s) where the address is "Park Lane 38": result:

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="rootroot",
  database="mydatabase"
)

mycursor = mydb.cursor()

sql = "SELECT * FROM customers WHERE address ='Park Lane 38'"

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)
  """
"""
#Select records where the address contains the word "way":
sql = "SELECT * FROM customers WHERE address LIKE '%way%'"

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)
  """
  """
  #Sort the result alphabetically by name: result:
  sql = "SELECT * FROM customers ORDER BY name"

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)
  """
  """
  #Sort the result reverse alphabetically by name:
  
sql = "SELECT * FROM customers ORDER BY name DESC"

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)
  """
  """
  #Delete any record where the address is "Mountain 21":


sql = "DELETE FROM customers WHERE address = 'Mountain 21'"

mycursor.execute(sql)

mydb.commit()

print(mycursor.rowcount, "record(s) deleted")

"""
"""
#You can delete an existing table by using the "DROP TABLE" statement:

sql = "DROP TABLE customers"

mycursor.execute(sql)
"""
"""
#If the the table you want to delete is already deleted, or for any other reason does not exist, you can use the IF EXISTS keyword to avoid getting an error.
#Delete the table "customers" if it exists:

sql = "DROP TABLE IF EXISTS customers"

mycursor.execute(sql)
"""
"""
#UPDATE
#Overwrite the address column from "Valley 345" to "Canyoun 123":

sql = "UPDATE customers SET address = 'Canyon 123' WHERE address = 'Valley 345'"

mycursor.execute(sql)

mydb.commit()

print(mycursor.rowcount, "record(s) affected")
"""
"""
#Escape values by using the placholder %s method:
sql = "UPDATE customers SET address = %s WHERE address = %s"
val = ("Valley 345", "Canyon 123")

mycursor.execute(sql, val)

mydb.commit()

print(mycursor.rowcount, "record(s) affected")
"""
"""
#Select the 5 first records in the "customers" table:

mycursor.execute("SELECT * FROM customers LIMIT 5")

myresult = mycursor.fetchall()

for x in myresult:
  print(x)
"""
"""
#Start from position 3, and return 5 records:

mycursor.execute("SELECT * FROM customers LIMIT 5 OFFSET 2")

myresult = mycursor.fetchall()

for x in myresult:
  print(x)
  """




